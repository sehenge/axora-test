<?php

namespace App\models;

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * Class Database
 */
class Database
{

    protected $handle;


    public function __construct()
    {
        $capsule = new Capsule();
        $capsule->addConnection(
            [
                'driver'    => 'mysql',
                'host'      => getenv('DB_HOST'),
                'username'  => getenv('DB_USER'),
                'password'  => getenv('DB_PASSWORD'),
                'database'  => getenv('DB_NAME'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
            ]
        );
        $capsule->bootEloquent();
    }
}
