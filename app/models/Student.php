<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Student
 * @package App\models
 */
class Student extends Eloquent
{

    public $name;

    /**
     * Student constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return object
     */
    public function getAllStudents(): object
    {
        $students = self::select('*')->orderBy('lastname', 'ASC')->get();

        return $students;
    }

    /**
     * @param $id
     * @return object
     */
    public function getStudentById($id): object
    {
        $student = self::select('*')->where('id', '=', $id)->first();

        return $student;
    }

    /**
     * @param $firstname
     * @param $middlename
     * @param $lastname
     * @param $dob
     * @return bool
     */
    public function addStudent($firstname, $middlename, $lastname, $dob): bool
    {
        $result = self::insert([
            'firstname'  => $firstname,
            'middlename' => $middlename,
            'lastname'   => $lastname,
            'dob'        => $dob
        ]);

        return $result;
    }

    /**
     * @param $id
     * @param $firstname
     * @param $middlename
     * @param $lastname
     * @param $dob
     * @return bool
     */
    public function editStudent($id, $firstname, $middlename, $lastname, $dob): bool
    {
        $result = self::where('id', $id)->update([
            'firstname'  => $firstname,
            'middlename' => $middlename,
            'lastname'   => $lastname,
            'dob'        => $dob
        ]);

        return $result;
    }

    /**
     * @param $id
     * @return bool
     */
    public function removeStudent($id): bool
    {
        $result = self::where('id', '=', $id)->delete();

        return $result;
    }
}
