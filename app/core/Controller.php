<?php

namespace App\core;

use App\models\Database;
use App\models\Student;

/**
 * Class Controller
 * @package App\core
 */
class Controller
{

    protected $db;
    protected $student;


    public function __construct()
    {
        $this->db = new Database();
        $this->student = new Student();
    }

    /**
     * @param $view
     * @param array $data
     */
    public function view($view, $data = [])
    {
        include_once 'app/views/' . $view . '.php';
    }
}
