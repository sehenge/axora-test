<?php

namespace App\core;

use Dotenv\Dotenv;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

/**
 * Class App
 * @package App\core
 */
class App
{

    protected $controller = 'IndexController';

    protected $method = 'index';

    protected $params = [];

    private $appName = 'App';


    public function __construct(string $appName = '')
    {
        session_start();
        $dotenv = Dotenv::createImmutable(__DIR__ . '/../../');
        $dotenv->load();
        $dotenv->required(['APP_NAMESPACE', 'APP_ENV', 'DB_NAME'])->notEmpty();
        if (getenv('APP_ENV') === 'dev')
        {
            $whoops = new Run;
            $whoops->pushHandler(new PrettyPageHandler);
            $whoops->register();
        }

        $this->appName = getenv('APP_NAMESPACE');

        $url = $this->parseUrl();
        if (isset($url[0]))
        {
            if (file_exists('app/controllers/' . ucfirst($url[0]) . 'Controller.php'))
            {
                $this->controller = ucfirst(strtolower($url[0])) . 'Controller';
                unset($url[0]);
            }
        }

        $classname = $this->appName . '\controllers\\' . $this->controller;
        $this->controller = new $classname;

        if (isset($url[1]))
        {
            if (method_exists($this->controller, $url[1] . 'Action'))
            {
                $this->method = $url[1] . 'Action';
                unset($url[1]);
            }
        }

        $this->params = ($url ?? array_values($url));
        call_user_func_array([$this->controller, $this->method], $this->params);
    }


    public function parseUrl(): array
    {
        if (isset($_GET['url']))
        {
            return $url = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
        }

        return [];
    }
}
