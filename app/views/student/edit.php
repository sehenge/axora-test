<?php
    $fullname = $data['student']->lastname . ' ' . $data['student']->firstname . ' ' . $data['student']->middlename;
?>
<h2 style="text-align:center;">Edit student</h2>
<h3 style="text-align:center;"><?php echo $fullname; ?></h3>
<div class="row pb-5 mb-4">
    <form action="/student/edit/<?php echo $data['student']->id;?>" method="POST" class="needs-validation" novalidate style="margin:auto auto;">
        <div class="form-group">
            <input type="text" class="form-control" id="lastname" placeholder="Lastname" name="lastname" pattern="[A-Za-zа-яА-Я]{1,20}" value="<?php echo $data['student']->lastname;?>" required>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" id="firstname" placeholder="Firstname" name="firstname" pattern="[A-Za-zа-яА-Я]{1,20}" value="<?php echo $data['student']->firstname;?>" required>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" id="middlename" placeholder="Middlename" name="middlename" pattern="[A-Za-zа-яА-Я]{1,20}" value="<?php echo $data['student']->middlename;?>" required>
        </div>
        <div class="form-group">
            <input type="date" class="form-control" id="dob" placeholder="Date of birth" name="dob" value="<?php echo $data['student']->dob;?>" required>
        </div>
        <div class="form-group">
            <button type="submit" value="Save" class="btn btn-primary" style="width:45%;">Save</button>
            <a href="/" class="btn btn-warning" role="button" style="width:45%;float:right;">Cancel</a>
        </div>
    </form>
</div>
</div>
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
