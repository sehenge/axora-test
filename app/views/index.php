<p class="font-italic text-muted mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
    incididunt.</p>
<?php

if (isset($_GET['msg'])) {
    $msg = [
        'type' => $_GET['type'],
        'msg'  => $_GET['msg'],
    ];
}
?>
<?php if (isset($msg)) : ?>
    <div class="alert alert-<?php echo $msg['type']; ?>">
        <?php echo $msg['msg']; ?>
    </div><br/>
<?php endif; ?>
<div class="row pb-5 mb-4" style="display:block;">
    <div><a href="/student/add" class="btn btn-primary" role="button" style="float:right;">Add student</a></div>
    <h3>School journal</h3>
</div>
<div class="row pb-5 mb-4">
    <?php
    if (!$data['students']->isEmpty()):
        ?>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Fullname</th>
                <th scope="col">Date of birth</th>
                <th scope="col">Age</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data['students'] as $student) {
                $fullname = "{$student['lastname']} {$student['firstname']} {$student['middlename']}";
                $date1 = new DateTime($student['dob']);
                $date2 = new DateTime();
                $age = $date1->diff($date2);
                echo '<tr>';
                echo '<th scope="row">' . $student['id'] . "</th>";
                echo '<td>' . $fullname . "</td>";
                echo '<td>' . date_format($date1, 'd.m.Y') . "</td>";
                echo '<td>' . $age->y . " years</td>";
                echo '<td>' . '<a href="/student/edit/' . $student['id'] . '" class="btn btn-info" role="button">Edit</a>' . "</td>";
                echo '<td>' . '<a href="/student/remove/' . $student['id'] . '" class="btn btn-danger" role="button">Remove</a>' . "</td>";
                echo '</tr>';
            }
            ?>

            </tbody>
        </table>
    <?php else: ?>
        <span class="badge badge-info">0 students found, please add some.</span>
    <?php endif; ?>
</div>
</div>
</body>
