<?php

namespace App\controllers;

use App\core\Controller;
use App\models\Student;

class IndexController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->view('header');
    }


    public function index(): void
    {
        $students = $this->student->getAllStudents();

        $this->view('index', ['students' => $students]);
    }
}
