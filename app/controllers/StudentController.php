<?php

namespace App\controllers;

use App\core\Controller;

/**
 * Class StudentController
 * @package App\controllers
 */
class StudentController extends Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->view('header');
    }


    public function index(): void
    {
        $students = $this->student->getAllStudents();

        $this->view('index', ['students' => $students]);
    }

    /**
     * @param $id
     */
    public function editAction($id): void
    {
        $student = $this->student->getStudentById($id);
        if (!empty($_POST['firstname']) && !empty($_POST['middlename']) && !empty($_POST['lastname']) && !empty($_POST['dob'])) {
            $result = $this->student->editStudent($id, $_POST['firstname'], $_POST['middlename'], $_POST['lastname'],
                $_POST['dob']);
            if ($result) {
                header('Location: ' . '/index?msg=Student successfully saved&type=success');
            } else {
                header('Location: ' . '/student/add?msg=Error with editing student&type=warning');
            }
        } else {
            $this->view('student/edit', ['student' => $student]);
        }
    }

    public function addAction(): void
    {
        if (!empty($_POST['firstname']) && !empty($_POST['middlename']) && !empty($_POST['lastname']) && !empty($_POST['dob'])) {
            $result = $this->student->addStudent($_POST['firstname'], $_POST['middlename'], $_POST['lastname'],
                $_POST['dob']);
            if ($result) {
                header('Location: ' . '/index?msg=Student successfully added&type=success');
            } else {
                header('Location: ' . '/student/add?msg=Error with adding student&type=warning');
            }
        } else {
            $this->view('student/add');
        }
    }

    /**
     * @param $id
     */
    public function removeAction($id): void
    {
        $result = $this->student->removeStudent($id);
        if ($result) {
            header('Location: ' . '/index?msg=Student successfully deleted&type=success');
        }
    }
}
